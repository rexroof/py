#!/usr/bin/env python
import json
import requests
import sys

try:
    ip       = sys.argv[1]
    ent_file = sys.argv[2]
except:
    print "pass ip and entity.json file on command line."
    exit(1)

print ip
print ent_file


# $ /usr/bin/curl -X POST -d blount.json -H "Content-Type:
# application/json" 

# es_url = 'http://127.0.0.1:3122/entities/upload'
# es_url = 'http://10.121.0.7:3122/entities/upload'
es_url = 'http://'+ip+':3122/entities/upload'
es_header = {'content-type': 'application/json'}


# import requests
# get_response = requests.get(url='http://google.com')
# post_data = {'username':'joeb', 'password':'foobar'}
# post_response = requests.post(url='http://some.other.site', data=post_data)



# f = open('entities20141212-085152.json', 'r+')
f = open(ent_file, 'r+')
j_obj = json.load(f)

for ent_obj in j_obj["entities"]:
    print ent_obj["name"]
    data_string = '['+json.dumps(ent_obj)+']'
    post_response = requests.post(es_url , headers=es_header, data=data_string)
    print post_response
